
let deals = [
	{
		title:'Javascript',
		description:'Learn it'
	},
	{
		title:'Find job',
		description:'As a Developer!'
	},
	{
		title:'Travel',
		description:'See the Eiffel Tower!!!'
	}
];


$(document).ready(function (){
	renderList();
	showCard();
	closeCard();
	AddButton();
	addCard();
	setNew();
	deleteDeal();
});


function renderList(){
	$.ajax({
		url: 'templates/list-group.html',
		success: function(listTemplate){
			let context = {deals: JSON.parse(localStorage.deals)};
			let template = Handlebars.compile(listTemplate);
			let html = template(context);

			console.log(JSON.parse(localStorage.deals));

			$('.place-for-list').html(html);

		}
	})
}


function showCard (){
$(document).on('click', '.get-card', function (){

	console.dir(this.dataset.key);

	let key = this.dataset.key;
	let deal = deals[key];

	console.dir(deal);

	$.ajax({
		url: 'templates/card.html',
		success: function (cardTemplate){

			let template = Handlebars.compile(cardTemplate);
			let html = template(deal);

			// console.dir(html);

			$('.place-for-card').html(html);
		}
	})

});
}

function closeCard () {
	$(document).on('click','.close',function(){
		$(this).parent().parent().parent().fadeOut();
	})
}

function AddButton () {
	$.ajax({
		url:'templates/add-button.html',
		success: function (addTemplate) {
			let template = Handlebars.compile(addTemplate);
			let html = template;

			$('.place-for-add').html(html);
		}
	})
}

function addCard (){
	$(document).on('click', '.add-card', function (){

		$.ajax({
			url: 'templates/add-card.html',
			success: function (cardTemplate){
				let template = Handlebars.compile(cardTemplate);
				let html = template;

				// console.dir(html);

				$('.place-for-card').html(html);
			}
		})

	});
}

function setNew () {
		$(document).on('submit','.add-form', function (e) {
			e.preventDefault();

			let newData = new FormData(this);

			deals.push({
				title: newData.get('title'),
				description: newData.get('description')
			});

			localStorage.setItem('deals', JSON.stringify(deals));
			renderList();

		});

}




function deleteDeal () {
	$(document).on('click', '.delete', function (e) {
		e.preventDefault();


     let deals = JSON.parse(localStorage.deals);


     deals.splice(this.dataset.key,1);

     localStorage.setItem('deals',JSON.stringify(deals));

     renderList();
		console.log(deals);

	});

}